<?php
require ('../../../config.php');
require ('../../../common.php');

$id = $_GET['Id'];
$sql = "SELECT Player.Id as PlayerId, FirstName, LastName, Email, Address1, Address2, PostalCode, City, Country, Phone, Birthday, Team.Name as TeamName FROM Player INNER JOIN Team ON Player.TeamId = Team.Id WHERE Player.Id = :Id";
// echo $insertSql;

try {
    $connection = new \PDO($host, $user, $password, $options);
    $statement = $connection->prepare($sql);
    $statement->bindParam(':Id', $id, PDO::PARAM_INT);
    $statement->execute();
    $result = $statement->fetch(PDO::FETCH_ASSOC);
} catch (\PDOException $e) {
    echo "Er is iets fout gelopen: {$e->getMessage()}";
}


include ('../../template/header.php'); 
?>
<main>
    <article>
        <header class="command-bar">
            <h2>Gegevensoverzicht van de speler</h2>
            <nav>
                <a class="icon-plus" href="InsertingOne.php"><span class="screen-reader-text">Inserting</span></a>
                <a class="icon-pencil" href="UpdatingOne.php?Id=<?php echo $id;?>"><span class="screen-reader-text">Updating</span></a>
                <a class="icon-bin" href="Delete.php?Id=<?php echo $id;?>"><span class="screen-reader-text">Updating</span></a>
                <a class="icon-cross" href="Index.php"><span class="screen-reader-text">Cancel</span></a>
            </nav>
        </header>
             <!-- form>(label+input:text)*6 -->
            <form action="" method="post">
                <fieldset>
                    <table>
                        <tr>
                            <th>Voornaam</th>
                            <td><?php echo $result ? $result['FirstName'] : ''?></td>
                        </tr>
                        <tr>
                            <th>Familienaam</th>
                            <td><?php echo $result ? $result['LastName'] : ''?></td>
                        </tr>
                        <tr>
                            <th>E-mail</th>
                            <td><?php echo $result ? $result['Email'] : ''?></td>
                        </tr>
                        <tr>
                            <th>Adres</th>
                            <td><?php echo $result ? $result['Address1'] : ''?></td>
                            <td><?php echo $result ? $result['Address2'] : ''?></td>
                        </tr>
                        <tr>
                            <th>Postcode</th>
                            <td><?php echo $result ? $result['PostalCode'] : ''?></td>
                        </tr>
                        <tr>
                            <th>Woonplaats</th>
                            <td><?php echo $result ? $result['City'] : ''?></td>
                        </tr>
                        <tr>
                            <th>Land</th>
                            <td><?php echo $result ? $result['Country'] : ''?></td>
                        </tr>
                        <tr>
                            <th>Telefoon</th>
                            <td><?php echo $result ? $result['Phone'] : ''?></td>
                        </tr>
                        <tr>
                            <th>Geboortedatum</th>
                            <?php $originalDate = 'Birthday'; ?>
                            <?php $newDate = date('d/m/Y', strtotime($originalDate)); ?>
                            <td><?php echo $newDate ?></td>
                        </tr>
                        <tr>
                            <th>Team</th>
                            <td><?php echo $result ? $result['TeamName'] : ''?></td>
                        </tr>
                    </table>
            </fieldset>
        </form>
        <div id="feedback">
            <?php 
               if (isset($_POST['submit']) && $statement) {
                    echo $newUser['Name'] . ' is toegevoegd.<br/>';
                }
                echo $sqlErrorMessage;
            ?>
        </div>
    </article>
    <aside>
        <?php include('ReadingAll.php');?>
    </aside>
    </main>
    
<?php include ('../../template/footer.php'); ?>