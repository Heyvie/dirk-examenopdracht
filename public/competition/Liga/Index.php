<?php 
require_once ('../../../config.php');
require_once ('../../../common.php');

include ('../../template/header.php'); ?>

<header>
<nav>
    <a href="../public/index.php">Back to home page</a>
</nav>
<h1>Competitie</h1>
</header>
<main>
    <article>
        <div class="command-bar">
            <h2>Liga</h2>
            <nav>
				<a class="icon-plus" href="InsertingOne.php"><span class="screen-reader-text">Inserting</span></a>
			</nav>
        </div>
    </article>
    <aside>
        <?php include('ReadingAll.php');?>
    </aside>
</main>

<?php include ('../../template/footer.php'); ?>