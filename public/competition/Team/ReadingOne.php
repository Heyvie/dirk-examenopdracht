<?php
require ('../../../config.php');
require ('../../../common.php');

$id = $_GET['Id'];
$sql = "SELECT Player.Id AS PlayerId, FirstName, LastName, Email, Address1, Address2, PostalCode, City, Country, Phone, Birthday, Team.Name as TeamName FROM Team INNER JOIN Player ON Team.PlayerId = Player.Id";
// echo $insertSql;

try {
    $connection = new \PDO($host, $user, $password, $options);
    $statement = $connection->prepare($sql);
    $statement->bindParam(':Id', $id, PDO::PARAM_INT);
    $statement->execute();
    $result = $statement->fetch(PDO::FETCH_ASSOC);
} catch (\PDOException $e) {
    echo "Er is iets fout gelopen: {$e->getMessage()}";
}


include ('../../template/header.php'); 
?>
<main>
    <article>
        <header class="command-bar">
            <h2>Liga</h2>
            <nav>
                <button type="submit" value="insert" form="form" name="submit" class="icon-floppy-disk"><span class="screen-reader-text">Insert</span></button></a>
                <a class="icon-cross" href="Index.php"><span class="screen-reader-text">Cancel</span></a>

            </nav>
        </header>
             <!-- form>(label+input:text)*6 -->
            <form action="" method="post">
               <fieldset>
                    <div>
                        <label for="FirstName">Voornaam</label>
                        <input type="text" name="FirstName" id="FirstName"
                            value="<?php echo $result ? $result['FirstName'] : ''?>
                    </div>
                    <div>
                        <label for="LastName">Familienaam</label>
                        <input type="text" name="LastName" id="LastName">
                    </div>
                    <div>
                        <label for="email">E-mail</label>
                        <input type="email" name="email" id="email">
                    </div>
                    <div>
                        <label for="Address1">Adres 1</label>
                        <input type="text" name="Address1" id="Address1">
                    </div>
                    <div>
                        <label for="Address2">Adres 2</label>
                        <input type="text" name="Address2" id="Address2">
                    </div>
                    <div>
                        <label for="PostalCode">Postcode</label>
                        <input type="text" name="PostalCode" id="PostalCode">
                    </div>
                    <div>
                        <label for="City">Stad</label>
                        <input type="text" name="City" id="City">
                    </div>
                     <div>
                        <label for="Country">Land</label>
                        <input type="text" name="Country" id="Country">
                    </div>   
                     <div>
                        <label for="Phone">Tel</label>
                        <input type="text" name="Phone" id="Phone">
                    </div>
                   <div>
                        <label for="Birthday">Geboortedatum</label>
                        <input type="date" name="Birthday" id="Birthday">
                    </div>
                    
                    <div>
                        <label for="TeamId">Team</label>
                        <select id="TeamId" name="TeamId">
                            <?php
                                if ($teamList) {
                                    foreach ($teamList as $teamRow) {
                            ?>
                                    <option value="<?php echo $teamRow['Id'];?>">
                                        <?php echo $teamRow['Name'];?>
                                    </option>
                            <?php
                                    }
                                }
                            ?>
                        </select>
                    </div>
                 
                    <input type="submit" value="Verzenden" name="submit">
            </fieldset>
        </form>
        <div id="feedback">
            <?php 
               if (isset($_POST['submit']) && $statement) {
                    echo $newUser['Name'] . ' is toegevoegd.<br/>';
                }
                echo $sqlErrorMessage;
            ?>
        </div>
    </article>
    <aside>
        <?php include('ReadingAll.php');?>
    </aside>
    </main>
    
<?php include ('../../template/footer.php'); ?>