DROP TABLE IF EXISTS `Player`;
DROP TABLE IF EXISTS `Game`;
DROP TABLE IF EXISTS `Liga`;
DROP TABLE IF EXISTS `Team`;

-- modernways.be
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql: CREATE TABLE Liga
-- Created on Wednesday 14th of November 2018 08:04:50 PM
-- 
CREATE TABLE `Liga` (
	`Name` NVARCHAR (50) NOT NULL,
	`Year` CHAR (4) NOT NULL,
	`IsInPlanning` BOOLEAN NULL,
	`Id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
	CONSTRAINT PRIMARY KEY(Id)
);