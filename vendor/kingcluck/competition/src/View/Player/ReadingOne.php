<header>
    <h1>Speler</h1>
    <a>Updating</a> <a>Delete</a> <a>Inserting</a>
</header>
<form>
    <fieldset>
        <legend>Speler</legend>
        <div>
            <label>Familienaam:</label>
            <input value="<?php echo $player->getLastName();?>" readonly />
        </div>
        <div>
            <label>Voornaam:</label>
            <input value="<?php echo $player->getFirstName();?>" readonly />
        </div>
        <div>
            <label>Adres:</label>
            <input value="<?php echo $player->getAddress();?>" readonly />
        </div>
        <div>
            <label>Rugnummer:</label>
            <input value="<?php echo $player->getShirtNumber();?>" readonly />
        </div>
    </fieldset>
</form>