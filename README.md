# Competition Project
30 JANUARI 2019

Dirk Heyvaert

Examenopdracht Programmeren 4
1. Maak een workspace met de naam voornaam-competition
2. Maak het model
3. Maak de views en de css
4. Maak de controller, voorlopig met een switch statement


## Support & Documentation

1. Voor de __leerstof__ zie [Modern Ways Programmeren 4](https://www.modernways.be/myap/it/school/course/Programmeren%204.html)
2. Voor **markdown** syntaxis zie [Markdown cheatsheet](https://github.com/adam-p/markdown-here/wiki/markdown-cheatsheet)


## Stappen
1. composer.json
2. run composer dumpautoload
3. maak de modelklassen voor KingCluck\Competition
4. maak de views voor KingCluck\Competition
5. maak de controller voor KingCluck\Competition 